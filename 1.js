const countChar = (string, subString) => {
    let count = 0
    string.replace(new RegExp(subString, 'gi'), () => count++)
    return count
}
console.log(countChar("kata", "a"))
